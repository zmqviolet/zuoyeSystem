import axios from "@/until/axios.js";
const checkAnswer = {
  data(){
    return{
      aid:0,
      subType:'',
      canAjax:true,
      showAnswer: true,  // 是否展开答案
      showWrong:0, // 0 表示正常答题，1表示check 2表示重做
      checkoutsData: {},  // 数据
      detailsData:{},// 查看详情页面数据
      isComplete: false, // 是否完成答题
    }
  },
  methods:{
    submitWrong(qid,pQid,sendValues) { // 提交check
      let parentQid;
      if (pQid) {
        parentQid = Number(pQid);
      } else {
        parentQid = 0;
      }
      let data = {
        aid: this.aid,
        qid: qid,
        parentQid: parentQid,
        subType: this.subType,
        answer: sendValues
      };
      let url = "/Exercise/checkAnswer";
      this.canAjax = false;
      axios
        .post(url, {
          ...data
        })
        .then(res => {
          if (res.code === 200) {
            this.$message({
              message: "check成功",
              type: "success"
            });
            this.detailsData = res.data;
            this.showWrong = 2;
          } else {
            this.$message({
              message: res.data.message,
              type: "error"
            });
          }
          this.canAjax = true;
        })
        .catch(err => {
          this.canAjax = true;
        });
    },
    agAinFun() {  // 重做错题
      this.isComplete = false;
      this.showWrong = 1;
    },
    displayAnswer() {  // 展开答案
      this.showAnswer = !this.showAnswer;
    },
  },
  created() {
    this.aid = this.$route.params.id;
    this.subType = this.$route.params.type;
  },
}
export default checkAnswer