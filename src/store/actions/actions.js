export default {
    // mutations是同步的，actions是异步的修改数据的,数据请求的，需要放在actions里面，简单的同步修改就直接放在mutations
    updateSingleDataASync(store, data) {
        setTimeout(() => {
            store.commit('updateCount',{num:data.num})
        },data.time)
    }
}