import Vuex from 'vuex'
import defaultState from './state/state'
import mutations from './mutations/mutations'
import getters from './getters/getters'
import actions from './actions/actions'
//入口文件,服务端渲染，每次都需要创建一个,每次服务端同一个会出问题

// 设置严格模式，就不能在mutations之外修改数据,这个最好不要在外部修改，会出现警告，正式环境不要设置这个，所以一般会这样写：
const isDev = process.env.NODE_ENV === 'dev'

export default () => {
  const store = new Vuex.Store({
    strict: isDev,
    state: defaultState,
    // 修改数据
    mutations,
    getters,
    actions,
    plugins: [
      (store) => {
        // 初始化的时候就执行
        console.log('plugins')
      }
    ]
  })
  // webpack热加载的功能
  if (module.hot) {
    module.hot.accept([
      './state/state',
      './mutations/mutations',
      './getters/getters',
      './actions/actions'
    ], () => {
      const newState = require('./state/state').default
      const newMutations = require('./mutations/mutations').default
      const newGetters = require('./getters/getters').default
      const newActions = require('./actions/actions').default
      store.hotUpdate({
        state: newState,
        mutations: newMutations,
        getters: newGetters,
        actions: newActions
      })
    })
  }
  return store
}
